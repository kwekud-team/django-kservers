from django.contrib.contenttypes.models import ContentType

from kservers.models import RequestLog, RequestLogObject


class BaseLogger:

    def __init__(self, site, server):
        self.site = site
        self.server = server
        self.memo = {}

    def init_request_log(self, action, api_response):
        request_log = RequestLog.objects.create(
            site=self.site,
            server=self.server,
            action=action,
            status_code=api_response.status_code,
            response_message=api_response.response_message,
            response_code=api_response.response_code,
        )
        self.memo[request_log.pk] = []
        return request_log

    def update_request_log(self, request_log, api_response=None):
        objects = self.memo[request_log.pk]
        total_count = len(objects)
        valid_count = len(list(filter(lambda x: x.is_valid, objects)))
        failed_count = total_count - valid_count
        all_valid = failed_count == 0

        if api_response:
            status_code = api_response.status_code
            response_message = api_response.response_message
            response_code = api_response.response_code
            response_body = api_response.response_body
        else:
            response_body = {}
            if all_valid:
                status_code = 200
                response_message = 'OK'
                response_code = 0
            else:
                response_message = 'An error occurred while handling your request'
                response_code = -1
                status_code = 400

        RequestLog.objects.filter(pk=request_log.pk).update(
            status_code=status_code,
            response_message=response_message,
            response_code=response_code,
            all_valid=all_valid,
            total_count=total_count,
            valid_count=valid_count,
            failed_count=failed_count,
            response_body=response_body
        )

    def save_request_log_object(self, request_log, content_object, api_response):
        content_type = ContentType.objects.get_for_model(content_object)

        request_log_object = RequestLogObject.objects.create(
            site=self.site,
            request_log=request_log,
            object_id=content_object.pk,
            content_type=content_type,
            is_valid=api_response.is_valid,
            response_message=api_response.response_message,
            response_code=api_response.response_code,
            response_body=api_response.response_body
        )
        self.memo[request_log.pk].append(request_log_object)
        return request_log_object
