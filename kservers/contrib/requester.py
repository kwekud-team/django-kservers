from kservers.models import Server, Service
from kservers.contrib.logger import BaseLogger
from django.conf import settings


class Requester:

    def __init__(self, site, identifier, base_url=None):
        self.site = site
        self.server = self.get_server(identifier, base_url=base_url)
        self.logger = BaseLogger(self.site, self.server)

    def get_service(self, identifier):
        return Service.objects.get_or_create(
            site=self.site,
            identifier=identifier
        )[0]

    def get_server(self, identifier, base_url=None):
        # if not identifier.strip():
        #     jk
        ttype = ('dev' if settings.DEBUG else 'live')
        return Server.objects.get_or_create(
            site=self.site,
            service=self.get_service(identifier),
            type=ttype,
            defaults={
                'base_url': base_url,
            })[0]
