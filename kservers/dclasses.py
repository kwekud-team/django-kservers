from dataclasses import dataclass, field, fields
from typing import List


@dataclass
class APIResponse:
    status_code: int
    response_message: str
    is_valid: bool
    response_code: str = ''
    request_body: dict = field(default_factory=dict)
    response_body: dict = field(default_factory=dict)

    def to_dict(self):
        return {f.name: getattr(self, f.name) for f in fields(self)}

    @classmethod
    def valid(cls, **kwargs):
        kw = {'status_code': 200, 'response_message': 'Success'}
        kw.update(dict(**kwargs))
        return APIResponse(is_valid=True, **kw)

    @classmethod
    def error(cls, **kwargs):
        kw = {'status_code': 500, 'response_message': 'Error handling request'}
        kw.update(dict(**kwargs))
        return APIResponse(is_valid=False, **kw)


@dataclass
class DetailAPIResponse(APIResponse):
    remote_id: str = ''
    remote_status: str = ''


@dataclass
class ListAPIResponse(APIResponse):
    response_body: List = field(default_factory=list)
