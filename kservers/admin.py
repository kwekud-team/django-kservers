from django.contrib import admin

from kservers.models import Service, Server, RequestLog, RequestLogObject


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'name', 'description', 'site')
    list_filter = ('site',)
    search_fields = ['name']


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ('pk', 'service', 'type', 'base_url', 'site')
    list_filter = ('is_active', 'type', 'site', )
    search_fields = ['name']


@admin.register(RequestLog)
class RequestLogAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'server', 'action', 'all_valid', 'total_count', 'valid_count', 'status_code',
                    'response_code', 'response_message', 'site')
    list_filter = ('date_created', 'action', 'all_valid', 'site')
    search_fields = ['action']


@admin.register(RequestLogObject)
class RequestLogObjectAdmin(admin.ModelAdmin):
    list_display = ('date_created', 'request_log', 'content_object', 'is_valid', 'response_code', 'response_message',
                    'section', 'site')
    list_filter = ('date_created', 'is_valid', 'site')
