from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class BaseModel(models.Model):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Service(BaseModel):
    name = models.CharField(max_length=250)
    identifier = models.CharField(max_length=100)
    description = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.name


class Server(BaseModel):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    type = models.CharField(max_length=250, help_text='eg: production, test, staging')
    base_url = models.CharField(max_length=250)
    settings = models.JSONField(default=dict)

    def __str__(self):
        return f'{self.service}: {self.type}'

    class Meta:
        unique_together = ['site', 'service', 'type']


class RequestLog(BaseModel):
    server = models.ForeignKey(Server, on_delete=models.CASCADE)
    action = models.CharField(max_length=50)
    all_valid = models.BooleanField(default=False)
    valid_count = models.PositiveIntegerField(default=0)
    failed_count = models.PositiveIntegerField(default=0)
    total_count = models.PositiveIntegerField(default=0)
    status_code = models.IntegerField()
    response_message = models.CharField(max_length=250)
    response_code = models.CharField(max_length=100, null=True, blank=True)
    response_body = models.JSONField(default=dict)

    def __str__(self):
        return self.response_message

    class Meta:
        ordering = ('-date_created',)


class RequestLogObject(BaseModel):
    request_log = models.ForeignKey(RequestLog, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey()
    section = models.CharField(max_length=250, null=True, blank=True)

    is_valid = models.BooleanField(default=False)
    response_message = models.CharField(max_length=250)
    response_code = models.CharField(max_length=100, null=True, blank=True)
    response_body = models.JSONField(default=dict)

    def __str__(self):
        return f'{self.content_object}: {self.response_message}'

    class Meta:
        ordering = ('-date_created',)
